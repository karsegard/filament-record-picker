<x-forms::field-wrapper :id="$getId()" :label="$getLabel()" :label-sr-only="$isLabelHidden()" :helper-text="$getHelperText()" :hint="$getHint()"
    :hint-action="$getHintAction()" :hint-color="$getHintColor()" :hint-icon="$getHintIcon()" :required="$isRequired()" :state-path="$getStatePath()">
    <div x-data="{
        state: $wire.entangle('{{ $getStatePath() }}'),
        openModal: () => {
            $dispatch('open-modal', { id: 'entity-picker', model:@js($getModel() ),statePath: '{{ $getStatePath() }}', selected: this.state,isMultiple:false,ownerRecordId:@js($getModelOwnerRecordId())})
        },
    
        onModalClose: (event) => {
            if (event.detail.statePath == '{{ $getStatePath() }}') {
                console.log(event)
                $wire.dispatchFormEvent('entity::attach', '{{ $getStatePath() }}', event.detail.record)
            }
        }
    }" class="space-y-6" x-on:close-modal.window="onModalClose($event)">

        <!-- Interact with the `state` property in Alpine.js -->
        <div class=" relative">
            <div class="absolute top-1 right-1">
                <button title="{{ __('forms::components.builder.buttons.delete_item.label') }}" type="button"
                wire:click=" dispatchFormEvent('entity::detach', '{{ $getStatePath() }}')"
                    @class([
                        'flex items-center justify-center flex-none w-10 h-10 text-danger-600 transition hover:text-danger-500',
                        'border-l border-b rounded-bl-xl',
                        'dark:text-danger-500 dark:hover:text-danger-400' => config(
                            'forms.dark_mode'
                        ),
                        'hidden'=>$isEmpty()
                    ])>
                    <span class="sr-only">
                        {{ __('forms::components.builder.buttons.delete_item.label') }}
                    </span>

                    <x-heroicon-s-trash class="w-4 h-4" />
                </button>
            </div>
            <div class="cursor-pointer " x-on:click="openModal()">
            {{ $getChildComponentContainer() }}
            </div>
        </div>

        {{--  <x-filament::button type="button" x-on:click="openModal()">
           pick
        </x-filament::button> --}}
    </div>
    @once
        @push('modals')
            {{-- <livewire:entity-picker modalId="entity-picker"/> --}}
            @livewire($getTableComponentName())
        @endpush
    @endonce
</x-forms::field-wrapper>
