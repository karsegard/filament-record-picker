{{-- <div x-data="{
    isOpen: false,
    selecteds: @entangle('selecteds'),
    statePath: @entangle('statePath'),
    isMultiple: @entangle('isMultiple'),
    openModal(event) {
        if (event.detail.id === '{{ $modalId }}') {
            console.log(event.detail)
            this.isOpen = true;
            this.statePath = event.detail.statePath;
            this.isMultiple = event.detail.isMultiple ? true : false;
            $wire.clearSelected();
        }
    },
}" role="dialog" aria-modal="true"
    x-on:close-modal.window="if ($event.detail.id === '{{ $modalId }}') isOpen = false;"
    x-on:open-modal.window="openModal($event)">
    <div x-show="isOpen" x-transition:enter="ease duration-300" x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100" x-transition:leave="ease duration-300" x-transition:leave-start="opacity-100"
        x-transition:leave-end="opacity-0" x-cloak
        class="fixed inset-0 z-40 flex items-center min-h-screen p-10 overflow-y-auto transition">
        <button x-on:click="isOpen = false; " type="button" aria-hidden="true"
            class="fixed inset-0 w-full h-full bg-black/50 focus:outline-none filament-curator-media-picker-modal-close-overlay"></button>

        <div
            class="max-h-full z-20 h-4/5- mx-auto p-2 space-y-2 bg-white rounded-xl cursor-default pointer-events-auto filament-modal-window w-4/5 max-w-6xl">
            <div class="px-4 py-2 filament-modal-header">
                <h2 class="text-xl font-bold tracking-tight filament-modal-heading">
                    Entities
                </h2>

                <div> --}}
<div x-data="{ model:@entangle('model'), selecteds: @entangle('selecteds'),isMultiple:@entangle('isMultiple'),type:@entangle('type'), statePath: @entangle('statePath') ,ownerRecordId:@entangle('ownerRecordId')}">
    <x-filament::modal 
        width="3xl" 
        id="entity-picker"
        x-on:open-modal.window="ownerRecordId=$event.detail.ownerRecordId;model=$event.detail.model ;statePath = $event.detail.statePath;type=$event.detail.type;$wire.clearSelected()">
        {{ $this->table }}
       

        
    </x-filament::modal>
</div>
{{--        </div>
                <x-filament::button type="button" wire:click="save">
                    Select {{count($selecteds)}} items
                </x-filament::button>
                
            </div>
        </div>
    </div>
</div>- --}}
