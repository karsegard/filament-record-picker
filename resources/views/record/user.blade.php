<x-dynamic-component :component="$getFieldWrapperView()" :id="$getId()" :label-sr-only="$isLabelHidden()" :helper-text="$getHelperText()" :hint="$getHint()"
    :hint-action="$getHintAction()" :hint-color="$getHintColor()" :hint-icon="$getHintIcon()" :required="$isRequired()" :state-path="$getStatePath()">
    <div x-data="{ state: $wire.entangle('{{ $getStatePath() }}').defer }">
        <!-- Interact with the `state` property in Alpine.js -->
        <div @class([
            'filament-forms-card-component p-6 bg-white rounded-xl border border-gray-300',
            'dark:border-gray-600 dark:bg-gray-800' => config('forms.dark_mode'),
        ])>
            <div class="flex flex-row gap-4 h-10 ">
                <x-heroicon-o-user class="max-h-full flex-initial"/>
                <div class="flex flex-col fit-content">
                    <span x-text="state.name"></span>
                    <span x-text="state.email"></span>
                </div>
            </div>
        </div>

    </div>
</x-dynamic-component>
