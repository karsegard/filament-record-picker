<?php

namespace KDA\Tests\Database\Factories;

use KDA\Tests\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    protected $model = Post::class;

    public function definition()
    {
        return [
            'title' => $this->faker->sentence(4),
        ];
    }
}
