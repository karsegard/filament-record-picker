<?php

namespace KDA\Filament\RecordPicker\Forms\Components;


use Filament\Forms\Components\ViewField;

class Record extends ViewField
{
    protected string $view = 'filament-record-picker::record.empty';

    public static function make(string $name = 'entity'): static
    {
        return parent::make($name);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->label('');
    }
}
