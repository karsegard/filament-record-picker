<?php

namespace KDA\Filament\RecordPicker\Forms\Components;

use Closure;
use Filament\Forms\Components\Concerns;
use Filament\Forms\Components\Field;
use Filament\Forms\Components\Hidden;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use KDA\Filament\RecordPicker\Livewire\EntityPicker;

class PickRecord extends Field
{
    use Concerns\HasHelperText;
    use Concerns\HasHint;
    use Concerns\CanBeValidated {
        required as traitRequired;
    }
    use Concerns\HasName;

    protected string $view = 'filament-record-picker::forms.components.pick-record';

    protected string $tableComponent;

    protected Closure $renderRecordUsing;

    

  
    public function tableComponent($string): static
    {
        $this->tableComponent = $string;

        return $this;
    }

    public function renderRecordUsing(Closure $component): static
    {
        $this->renderRecordUsing = $component;

        return $this;
    }

    public function getRenderRecord()
    {
        $state = $this->getState();
        $record = null;

        if (!blank($state)) {
            $record = $this->getRelatedModel($state['id']);
        }

        return $this->evaluate($this->renderRecordUsing, ['record' => $record]);
    }

    public function getTableComponent(): string
    {
        return this->tableComponent;
    }

    public function getTableComponentName(): string
    {
        return $this->tableComponent::getName();
    }

    public function getChildComponents(): array
    {
        return [
            /*   Hidden::make($typeColumn)->required($this->isRequired()),
            Hidden::make($keyColumn)->required($this->isRequired()),
*/
            $this->getRenderRecord(),
        ];
    }

    public function getRelationship(): BelongsTo | HasOne
    {
        return $this->getModelInstance()->{$this->getName()}();
    }

    public function getModelOwnerRecordId(): mixed {
        return $this->getRelationship()->getParent()->getKey();
    }
    public function getModel():string 
    {
        return get_class($this->getRelationship()->getRelated());
    }

   /* public function state($state):static{

        if(is_array($state)){
            throw new \Exception("prout");
        }
        
        return parent::state($state);
    }*/
    

    public function getRelatedModel( $id): ?Model
    {
      /*  if (! blank($type)) {
            return $type::find($id);
        }
*/
        return $this->getModel()::find($id);
    }

    public function required(Closure|bool $condition = true): static
    {
        $this->traitRequired($condition);
        if ($condition === true) {
            $this->rules([
                function () {
                    return function (string $attribute, $value, Closure $fail) {
                        if (empty($value['id']) || empty($value['type'])) {
                            $fail("The {$attribute} is required.");
                        }
                    };
                },
            ]);
        }

        return $this;
    }

    public function isEmpty()
    {
        $state = $this->getState();

        return   empty($state);
    }

    public static function defaultState($model): array
    {
        if ($model) {
            return $model->getKey();
        }

        return null;
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->tableComponent(EntityPicker::class);

        $this->default(null);

        $this->loadStateFromRelationshipsUsing(static function ($component, $record): void {
            $relationship = $component->getRelationship();
           // $typeColumn = $relationship->getMorphType();
            $keyColumn = $relationship->getForeignKeyName();
            $component->state([
                'id'=> $record->{$keyColumn},
                'entity'=>$component->getRelatedModel($record->{$keyColumn})?->toArray(),
            ]);
        });

        $this->saveRelationshipsUsing(static function ($component, $state, $record, $get) {
            $relationship = $component->getRelationship();
          //  $typeColumn = $relationship->getMorphType();
            $keyColumn = $relationship->getForeignKeyName();

            $record->update([
           //     $typeColumn => $state['type'],
                $keyColumn => $state,
            ]);

            return $state;
        });

        $this->registerListeners([
            'entity::attach' => [
                function ($component, string $statePath, $selected): void {
                    if ($component->isDisabled()) {
                        return;
                    }

                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }
                    /*foreach ($entity as $en) {
                        $this->attachEntity($en);
                    }*/
                    //dump($selected); 
                    $this->attachEntity($selected);
                },
            ],
            'entity::detach' => [
                function ($component, string $statePath): void {
                    if ($component->isDisabled()) {
                        return;
                    }

                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }
                    $this->state($this->defaultState(null));
                },
            ],
        ]);
    }

    protected function attachEntity($entity)
    {
        // $set = $this->getSetCallback();
        $relationship = $this->getRelationship();
        // $typeColumn = $relationship->getMorphType();
        // $keyColumn = $relationship->getForeignKeyName();
        $model = $this->getRelatedModel($entity);

      //  $this->state(array_merge($entity, ['entity' => $model->toArray()]));
     // $this->state($entity);
        $this->state([
            'id'=>$entity,
            'entity'=>$model->toArray()
        ]);
        /*   $set($typeColumn, $entity['type']);
         $set($keyColumn, $entity['id']);*/
    }
}
