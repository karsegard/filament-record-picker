<?php

namespace KDA\Filament\RecordPicker\Livewire;

use Closure;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\Layout;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Contracts\Database\Eloquent\Builder;
use KDA\Laravel\Entity\Collection\Models\Entity;
use KDA\Laravel\Entity\Collection\Models\EntityType;
use Livewire\Component;
use App\Models\User;

class EntityPicker extends Component implements Tables\Contracts\HasTable
{
    use Tables\Concerns\InteractsWithTable;

    public $modalId;

    public $selected = null;

    public $ownerRecordId= null;

    public $statePath = null;

    public $isMultiple = false;
    public $model=null;

    public $type = null;

    protected $listeners = ['refreshComponent' => '$refresh'];

    protected function getTableQuery(): Builder
    {
        //dump($this->model);

        if(!blank($this->model)){
            return $this->model::query(); 
        }
        return User::query();
    }

    protected function getTableColumns(): array
    {

        return [
            TextColumn::make('name')
                ->searchable()
                ->action(function ($record): void {
                  
                  /*  $selected = isset($this->selecteds[$record->getKey()]);

                    if ($selected) {
                        unset($this->selecteds[$record->getKey()]);
                    } else {
                        $this->selecteds[$record->getKey()] = true;
                    }
*/
                    $this->selected=$record->getKey();
  //                  if (! $this->isMultiple) {
                        $this->dispatchBrowserEvent('close-modal', [
                            'id' => 'entity-picker',
                            'record' => $this->selected,
                            'statePath' => $this->statePath, ]);
    //                }
                }),
        ];
    }

    protected function getTableRecordClassesUsing(): ?Closure
    {
        return fn ($record) => match (isset($this->selecteds[$record->getKey()])) {
            true => 'bg-gray-500',
            default => null,
        };
    }

    protected function getTableFilters(): array
    {
       // $options = EntityType::where('name', $this->type)->first()?->modelClasses->pluck('name', 'class')->toArray();

        return [
            // ...
         /*   SelectFilter::make('model_type')->visible(fn () => $this->type !== null && null !== $options && count($options) > 1)->options(function ($livewire) {
                $options = EntityType::where('name', $livewire->type)->first()?->modelClasses->pluck('name', 'class')->toArray();

                if (blank($options)) {
                    $options = [];
                }

                return $options;
            }),*/
        ];
    }

    public function save()
    {
        $this->dispatchBrowserEvent('close-modal', [
            'id' => $this->modalId,
            'entities' => $this->selecteds,
            'statePath' => $this->statePath, ]);
    }

    public function render()
    {
        return view('filament-record-picker::livewire.entity-picker');
    }

    public function clearSelected()
    {
        $this->selecteds = [];
        $this->selected = null;
        $this->emit('refreshComponent');
        $this->resetTableFiltersForm();
        $this->setPage(1);
    }
}
